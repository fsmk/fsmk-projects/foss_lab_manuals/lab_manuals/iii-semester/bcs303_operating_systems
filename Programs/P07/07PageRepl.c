#include <stdio.h>

#define MAX_FRAMES 3 // Number of frames in memory

// Function to simulate FIFO Page Replacement Algorithm
void fifo(int pages[], int n, int capacity) {
    int frame[MAX_FRAMES]; // Array to store frames
    int frameCount = 0;    // Count of frames currently in use
    int pageFaults = 0;    // Count of page faults

    for (int i = 0; i < n; i++) {
        int page = pages[i];
        int pageExists = 0;

        // Check if page is already in a frame
        for (int j = 0; j < frameCount; j++) {
            if (frame[j] == page) {
                pageExists = 1;
                break;
            }
        }

        // If page is not in a frame, replace the oldest page
        if (!pageExists) {
            if (frameCount < capacity) {
                // If there are empty frames, use them
                frame[frameCount++] = page;
            } else {
                // Replace the oldest page (first-in)
                for (int j = 0; j < capacity - 1; j++) {
                    frame[j] = frame[j + 1];
                }
                frame[capacity - 1] = page;
            }
            pageFaults++;
        }

        // Print the current state of frames for FIFO
        printf("FIFO - Page %d: ", page);
        for (int j = 0; j < frameCount; j++) {
            printf("%d ", frame[j]);
        }
        if(!pageExists)	printf("\tFault");
        printf("\n");
    }

    printf("Total Page Faults (FIFO): %d\n", pageFaults);
}

// Function to simulate LRU Page Replacement Algorithm
void lru(int pages[], int n, int capacity) {
    int frame[MAX_FRAMES]; // Array to store frames
    int frameCount = 0;    // Count of frames currently in use
    int pageFaults = 0;    // Count of page faults
    int counter[MAX_FRAMES]; // Counter to store the age of each page in a frame

    for (int i = 0; i < n; i++) {
        int page = pages[i];
        int pageExists = 0;

        // Check if page is already in a frame
        for (int j = 0; j < frameCount; j++) {
            if (frame[j] == page) {
                pageExists = 1;
                counter[j] = 0; // Reset the age of the used page
                break;
            }
        }

        // If page is not in a frame, replace the least recently used page
        if (!pageExists) {
            if (frameCount < capacity) {
                // If there are empty frames, use them
                frame[frameCount] = page;
                counter[frameCount] = 0;
                frameCount++;
            } else {
                // Find the page with the maximum age
                int maxAgeIndex = 0;
                for (int j = 1; j < capacity; j++) {
                    if (counter[j] > counter[maxAgeIndex]) {
                        maxAgeIndex = j;
                    }
                }

                // Replace the least recently used page
                frame[maxAgeIndex] = page;
                counter[maxAgeIndex] = 0;
            }
            pageFaults++;
        }

        // Increment the age of all pages in frames
        for (int j = 0; j < frameCount; j++) {
            counter[j]++;
        }

        // Print the current state of frames for LRU
        printf("LRU - Page %d: ", page);
        for (int j = 0; j < frameCount; j++) {
            printf("%d ", frame[j]);
        }
        if(!pageExists)	printf("\tFault");        
        printf("\n");
    }

    printf("Total Page Faults (LRU): %d\n", pageFaults);
}

int main() {
    int pages[] = {7, 0, 1, 2, 0, 3, 0, 4, 2, 3, 0, 4};
    int n = sizeof(pages) / sizeof(pages[0]);
    int capacity = 3;

    printf("Page Reference String: ");
    for (int i = 0; i < n; i++) {
        printf("%d ", pages[i]);
    }
    printf("\n\n");

    fifo(pages, n, capacity);
    printf("\n");
    lru(pages, n, capacity);

    return 0;
}

